FROM openjdk:8-jre

ENV HAWTIO_VERSION 1.4.68

COPY hawtio-app-${HAWTIO_VERSION}.jar /hawtio-app.jar

CMD /usr/bin/java -jar /hawtio-app.jar
